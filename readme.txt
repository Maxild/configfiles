The files can by symlinked to there destinations

Ex: mklink <link-dst> <target-src> => soft link (symlink)

mklink C:\Users\mom\.gitconfig C:\Config\.gitconfig
mklink C:\Users\mom\mercurial.ini C:\Config\mercurial.ini

GEM

This wil work

gem install bundler --http-proxy http://bluecoat.brfkredit.brf:8080 --source http://rubygems.org/

or 

gem install bundler -p http://bluecoat.brfkredit.brf:8080 -s http://rubygems.org/

Set the following environment variable: HTTP_PROXY or HTTPS_PROXY

And configure rubygems

gem sources -a http://rubygems.org/
gem sources -r https://rubygems.org/

see also: https://gist.github.com/luislavena/f064211759ee0f806c88

This cal also be helpful

gem update --system

NODEJS debugging

Install Node.js build tool:

npm install -g node-gyp

The build tool has a few requiremnts:

1) Make sure Python 2.7.8 is installed.
2) Make sure the Windows 7 64-bit SDK is installed (if not uninstall VC++ 10.0 Redistrbutable, install it, and reinstall VC++ 10.0 Redistrbutable).

Install the debugger:

npm install -g node-inspector

To debug gulpfile with node-insspector and break on the first line (--debug-brk is implicit/default)

node-debug gulp clean

If 'debugger' statemenet, or breakpoint is all ready in code

node-debug --debug-brk=0 gulp clean

NPM

npm config set proxy http://bluecoat.brfkredit.brf:8080
npm config set https-proxy http://bluecoat.brfkredit.brf:8080

npm config set registry http://registry.npmjs.org/

When searching for modules NODE_PATH env variable can be used.

On my windows install I use npm from the directory 'C:\Users\mom\AppData\Roaming\npm'. This
is non standard, but enables me to update npm using npm. For this to work I have defined

prefix = C:\Users\mom\AppData\Roaming\npm in .npmrc (i.e. installing global modules work)
NODE_PATH = C:\Users\mom\AppData\Roaming\npm (i.e. searching for global modules work)

When the global flag is set, npm installs things into this prefix.
The prefix config defaults to the location where node is installed. On windows, this is the exact location of the node.exe binary.

My PATH has the following order

1) C:\Users\mom\AppData\Roaming\npm
2) C:\Program Files\nodejs (npm is installed next to node.exe on windows)

That is C:\Users\mom\AppData\Roaming\npm is added in front of C:\Program Files\nodejs in the PATH env variable.

Write this in CMD

npm root -g (prints the effective node_modules folder)

to check the prefix.

See also https://github.com/joyent/node/issues/2207

========================================
NodeJS and NPM are hard to on Windows!!!
========================================

long path issue: https://github.com/joyent/node/issues/6960 is depressing reading.

Node works fine on Windows, right up until it doesn't, and there's no way to know when it will work and when it won't without actually installing a module.  There's no guarantee that a module that works fine at version 0.1.0 will continue to be working at version 0.2.0, as it only takes a single added dependency that itself has a deep dependency tree to break things.

Because python and git (prior to 1.9) do not handle long paths properly, node-gyp will fail to build things deep in the tree. Windows API path limitations make npm unusable, because some npm modules use Visual Studio to build files. 

Tools that can work with long paths

- node
- npm
- msysgit (turn on long paths)
- SourceTree
- 7zip (zip your folder, move the zipped file and unzip it without any issue)
- Use rimraf to delete node_modules when you want a fresh install
- After doing 'npm install', do 'npm dedupe', then create a tarball of node_modules. Check this into source control for CI and/or disaster recovery purposes.
- Incorporate as part of the CI build script a task that untars the node_modules tarball into node_modules before building.


Tools that do not support long paths

- cmd
- Explorer 
- PowerShell
- .NET managed code (See http://blogs.msdn.com/b/bclteam/archive/2007/02/13/long-paths-in-net-part-1-of-3-kim-hamilton.aspx)
- Visual Studio
- MSBuild: \Microsoft.CppBuild.targets(254,5): error MSB3371: The file "Release\obj\u serid\userid.unsuccessfulbuild" cannot be created. The specified path, file name, or both are too long. The fully qualified  file name must be less than 260 characters, and the directory name must be less than 248 characters.

Hacky solutions involve a few rounds of the following: npm install, npm dedupe, npm shrink, and rimraf node_modules. Doing this repeatedly seems to iron out the long file paths to some degree, but it's painfull!

Other tools: 
- https://github.com/zetlen/fenestrate
- https://github.com/arifsetiawan/flatten

Node's module loader is probably the most frozen piece of its API surface, and it's the primary abstraction that connects the 80k+ modules that form the Node module ecosystem. npm has a certain amount of freedom to restructure how it installs modules in projects, as long as it follows the Node module system's rules; if it has that freedom but Node doesn't, why shouldn't it be the piece to change?

IMPORTANT:
==========
From Issac: We are working on changing the way that npm lays modules out on the file system. Node is not the faulty technology here, it has nothing to do with package installation.

BOWER

No bower config command in CLI, therefore...

Set the following environment variable:  HTTP_PROXY or HTTPS_PROXY, or

Bowerrc:

{
 "proxy" : "http://bluecoat.brfkredit.brf:8080",
 "https-proxy" : "http://bluecoat.brfkredit.brf:8080",
 "strict-ssl": false
}

GIT

git config --global http.proxy http://bluecoat.brfkredit.brf:8080
git config --global https.proxy http://bluecoat.brfkredit.brf:8080
git config --global http.sslVerify false
git config --global url."http://".insteadOf git://

To enable long path (> 260) support for builtin commands in Git for
Windows. This is disabled by default, as long paths are not supported
by Windows Explorer (by design, MS will not change that), cmd.exe and
the Git for Windows tool chain	(msys, bash, tcl, perl...). Only enable this if you know what you're doing and are prepared to live with a few quirks.

git config core.longpaths true (not recommended out of the box for every repo)

BUT do not checkin your node_modules folder, because npm handles long paths
without any problem on windows. 

FYI, if you want to remove very long paths in Windows, you can 'npm install -g rimraf' and then use the rimraf command line bin it creates. Works just like rm -rf on Unix.

Hacks to fight aginst long paths (deep dependency tree):
========================================================

- 'npm ls' to get a list of all installed modules (find duplicates).
- 'npm install module@version --save-dev' to install duplicate modules in the root node_modules directory and update package.json.
- rimraf node_modules to delete the node_modules directory.
- 'npm install' to pull down a fresh copy of your dependencies.

or

- Try using 'npm dedupe' to optimize your directory hierarchy which may shorten some paths ('npm dedupe' will only flatten the "common" modules to the lowest common location in the hierarchy. Not always good enough).
- Use 'npm install --production' to install without the development tools


HG

hg --config http_proxy.host=bluecoat.brfkredit.brf:8080 
hg --config http_proxy.user=[username] 
hg --config http_proxy.passwd=[password]

PROTRACTOR

OBS: Starting with protractor 1.0.x the default npm environment settings are used, but I couldn't get it to work.

This worked:

webdriver-manager update --ignore_ssl --proxy=http://bluecoat.brfkredit.brf:8080 --standalone

But why did I have to copy from

C:\Users\mom\AppData\Roaming\npm\node_modules\protractor\selenium

to

C:\Dev\Web\Projects\yo-gulp-angular\node_modules\protractor\selenium

in order for 'gulp protractor' to work???